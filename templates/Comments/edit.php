<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Comment $comment
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $comment->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $comment->id), 'class' => 'side-nav-item']
            ) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="comments form content">
            <?= $this->Form->create($comment) ?>
            <fieldset>
                <legend><?= __('Edit Comment') ?></legend>
                <?php
                    echo $this->Form->control('post_id', ['options' => $posts, 'type' => 'hidden']);
                    echo $this->Form->control('user_id', ['options' => $users, 'type' => 'hidden']);
                    echo $this->Form->control('message', [
                        'class' => 'form-control',
                        'label' => '',
                        ]
                    );
                ?>
            </fieldset>
            <?= $this->Form->button('Submit', ['class' => 'btn btn-primary mt-1']) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
