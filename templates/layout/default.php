<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @since         0.10.0
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 * @var \App\View\AppView $this
 */

$cakeDescription = 'CakePHP: the rapid development php framework';
?>
<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Microblog</title>
    <?= $this->Html->meta('icon') ?>
    <?= $this->Html->meta('csrfToken', $this->request->getAttribute('csrfToken')); ?>

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,700" rel="stylesheet">

    <?= $this->Html->script([
        'infinite',
        'jquery-3.6.0.min',
        'bootstrap',
        'bootstrap.min',
        'bootstrap.bundle',
        'bootstrap.bundle.min',

    ]) ?>
    <?= $this->Html->css([
        'bootstrap',
        'bootstrap.min',        
        'bootstrap-responsive',
        'bootstrap-responsive.min',
        'bootstrap-grid',
        'bootstrap-grid.min',
        'bootstrap-responsive',
        'bootstrap-responsive.min',
        ])
    ?>
    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
</head>
<body>
<div class="container" style="margin-bottom:80px">
<nav class="navbar fixed-top navbar-expand-lg navbar-light bg-light">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="<?= $this->Url->build('/') ?>">
        <?php 
            echo $this->Html->image(
            'microblog-logo.png', array(
                'alt' => 'MicroblogByJPT',
                'border' => '0',
                'data-src' => 'holder.js/100%x100',
                'width' => '50px'
                )
            );
        ?>
  </a>
    <?php if ($this->Identity->isLoggedIn()) : ?>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo03" style="width:100%">
          <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <li class="nav-item active">
                <a class="nav-link" href="<?= $this->Url->build('/') ?>">Home</a>
            </li>
            <li>
            <?= $this->Form->create(null,[
                'controller' => 'searches',
                'action' => 'search',
                'valueSources' => 'query',
                'id' => 'searchKey'
                ]
            ); ?>
            <?php
                echo $this->Form->input('', [
                    'id' => 'search',
                    'name' => 'search',
                    'placeholder' => 'search',
                    'type' => 'search',
                    'class' => 'form-control mr-sm-2'
                ]);
                echo $this->Form->end();
            ?>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $this->Url->build([
                    'controller' => 'posts',
                    'action' => 'myPosts'
                    ]) ?>">My Posts</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $this->Url->build([
                    'controller' => 'follows',
                    'action' => 'index'
                    ]) ?>">Follows</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $this->Url->build([
                    'controller' => 'users',
                    'action' => 'view',
                    $this->Identity->get('id')
                    ]) ?>"><?= utf8_decode($this->Identity->get('user_name')) ?>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="<?= $this->Url->build([
                    'controller' => 'users',
                    'action' => 'logout'
                    ]) ?>">Log Out
                </a>
            </li>
          </ul>
        </div>
    <?php endif ?>
</nav>

</div>

    <main class="main">
        <div class="container">
            <?= $this->Flash->render() ?>
            <?= $this->fetch('content') ?>
        </div>
    </main>
    <footer>
    </footer>
</body>
<script>
  $(function(){
    $("#searchKey").on("submit", function(e){
      var searhKey = $("input[name=search]");
      if (searhKey.val() == '') {
        searhKey.addClass("alert-danger");
        e.preventDefault();
      }
    });
  });
</script>
</html>
