<?php
/** 
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Like $like
 */
?>
<div class="row">
  <aside class="column">
    <div class="side-nav">
      <h4 class="heading"><?= __('Search Result') ?></h4>
    </div>
  </aside>
  <div class="column-responsive column-80">
    <div class="users view content">
        <?= (count($users) === 0) ? 'No user found.<br>' :'User/s found.' ?>
        <?php foreach ($users as $user) : ?>
            <div class="row">
              <div class="col-md-1">
              <a href="<?= $this->Url->build(['controller' => 'users', 'action' => 'view' , $user->id])?>">
                    <?php 
                        echo $this->Html->image(
                            'photos/' . $user->photo, [
                                'alt' => 'MicroblogByJPT',
                                'style' => 'max-width:50px;max-height:50px;width:auto;height:auto'
                            ]);
                    ?>
                </a>
              </div>
              <div class="col-md-8">
                  <?php
                      $fullName = utf8_decode($user->full_name);
                      echo $this->Html->link(__($fullName), [
                          'controller' => 'users',
                          'action' => 'view',
                          $user->id
                      ]);
                  ?>
              </div>
            </div>
        <?php endforeach ?>
            
        <?= (count($posts) === 0) ? 'No post found.' :'Post/s found.' ?>
        <?php foreach ($posts as $post) : ?>
            <div class="row">
              <div class="col-md-12">
                <a href="<?= $this->Url->build(['controller' => 'posts', 'action' => 'view' , $post->id])?>">
                  <?= utf8_decode($post->body) ?>
                </a>
              </div>
            </div>
        <?php endforeach ?>
    </div>
  </div>
</div>
