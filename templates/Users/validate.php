<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
  <div class="column-responsive column-80">
    <div class="users form content">
        <?= $this->Form->create($user) ?>
        <fieldset>
            <legend><?= __('Activation for Microblog') ?></legend>
                <?php
                    echo $this->Form->control('email', ['readonly', 'class' => 'form-control']);
                    echo $this->Form->control('input_activation_code', [
                        'label' => 'Activation Code',
                        'class' => 'form-control'
                        ]);
                ?>
        </fieldset>
            <?= $this->Form->button('Submit', [
              'type' => 'submit',
              'name' => 'submit',
              'id' => 'submit',
              'class' => 'btn btn-primary mt-1',
              'value' => 'validate'
              ]);?>
            <?= $this->Form->button('Request New Activation Code', [
              'type' => 'submit',
              'name' => 'submit',
              'id' => 'resend',
              'class' => 'btn btn-secondary mt-1',
              'value' => 'resend'
              ]);?>
            <?= $this->Html->link(__('Login'), ['action' => 'login']) ?>
            <?= $this->Form->end() ?>
    </div>
  </div>
</div>

<script>
  $(function(){
    $("#submit").on("click", function(e){
      var userName = $("input[name=input_activation_code]");
      if (userName.val() == '') {
        userName.addClass("alert-danger");
        e.preventDefault();
      }
    });
  });
</script>