<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
  <div class="column-responsive d-flex justify-content-center">
    <div class="users form content">
        <?= $this->Form->create($user, ['type' => 'file', 'action' => 'add']) ?>
        <fieldset>
          <legend><?= __('Sign Up to MicroblogByJPT') ?></legend>
            <?php
                echo $this->Form->control('photo', [
                    'type' => 'file',
                    'accept' => 'image/*',
                    'class' => 'form-control',
                    'required' => false
                ]);
                echo $this->Form->control('user_name', ['class'=>'form-control','required' => false]);
                echo $this->Form->control('full_name', ['class'=>'form-control','required' => false]);
                echo $this->Form->control('email', ['class'=>'form-control','required' => false]);
                echo $this->Form->control('password', ['class'=>'form-control','required' => false]);
                echo $this->Form->control('confirm_password',[
                    'type' => 'password',
                    'class'=>'form-control',
                    'required' => false
                    ]
                );
            ?>
        </fieldset>
        <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary mt-1']) ?>
        <?= $this->Html->link(__('Login'), ['action' => 'login']) ?>
        <?= $this->Form->end() ?>
    </div>
  </div>
</div>
<script>
  $(function(){
    $("form").on("submit", function(e){
      var photo = $("input[name=photo]");
      var userName = $("input[name=user_name]");
      var fullName = $("input[name=full_name]");
      var email = $("input[name=email]");
      var password = $("input[name=password]");
      var confirmPassword = $("input[name=confirm_password]");
      var check = true;
      
      if (photo.val() == '') {
        photo.addClass("alert-danger");
        check = false;
      } else {
        photo.removeClass("alert-danger").addClass("alert-success");
      }
      if (userName.val() == '') {
        userName.addClass("alert-danger");
        check = false;
      } else {
        userName.removeClass("alert-danger").addClass("alert-success");
      }
      if (fullName.val() == '') {
        fullName.addClass("alert-danger");
        check = false;
      } else {
        fullName.removeClass("alert-danger").addClass("alert-success");
      }
      if (email.val() == '') {
        email.addClass("alert-danger");
        check = false;
      } else {
        email.removeClass("alert-danger").addClass("alert-success");
      }
      if (password.val() == '') {
        password.addClass("alert-danger");
        check = false;
      } else {
        password.removeClass("alert-danger").addClass("alert-success");
      }
      if (confirmPassword.val() == '') {
        confirmPassword.addClass("alert-danger");
        check = false;
      } else {
        confirmPassword.removeClass("alert-danger").addClass("alert-success");
      }
      if (!check) {
        e.preventDefault();
      }
    });
  });
</script>