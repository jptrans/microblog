<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<?php if (!$forPasswordChange) { ?>
  <div class="row">
    <div class="column-responsive d-flex justify-content-center">
        <div class="users form content">
            <?= $this->Form->create($user, ['id' => 'passwordRecovery']) ?>
            <fieldset>
                <legend><?= __('Password recovery for Microblog') ?></legend>
                    <?php
                        echo $this->Form->control('email', [
                            'class' => 'form-control',
                            'required' => false
                            ]
                        );
                    ?>
            </fieldset>
                <?= $this->Form->input('_method', [
                  'type' => 'hidden',
                  'name' => '_method',
                  'value' => 'PUT'
                ]) ?>
                <?= $this->Form->button('Get Verification Code', [
                'type' => 'submit',
                'name' => 'submit',
                'class' => 'btn btn-secondary mt-1',
                'value' => 'forgot'
                ]) ?>
                <?= $this->Html->link(__('Login'), ['action' => 'login']) ?>
                <?= $this->Form->end() ?>
        </div>
    </div>
  </div>
<?php } else { ?>
  <div class="row">
    <div class="column-responsive d-flex justify-content-center">
        <div class="users form content">
            <?= $this->Form->create($user) ?>
            <fieldset>
                <legend><?= __('Password recovery for Microblog') ?></legend>
                    <?php
                        echo $this->Form->control('email', [
                            'class' => 'form-control',
                            'readonly'
                            ]
                        );
                        echo $this->Form->control('new_password', [
                            'type' => 'password',
                            'class' => 'form-control',
                            ]
                        );
                        echo $this->Form->control('confirm_new_password', [
                            'type' => 'password',
                            'class' => 'form-control'
                            ]
                        );
                        echo $this->Form->control('verification_code', [
                            'class' => 'form-control'
                            ]
                        );
                    ?>
            </fieldset>
                <?= $this->Form->button('Submit', [
                    'type' => 'submit',
                    'name' => 'submit',
                    'id' => 'changePassword',
                    'class' => 'btn btn-primary mt-1',
                    'value' => 'changePassword'
                ]);?>
                <?= $this->Form->button('Get New Verification Code', [
                    'type' => 'submit',
                    'name' => 'submit',
                    'class' => 'btn btn-secondary mt-1',
                    'value' => 'resendVerificationCode'
                ]);?>
                <?= $this->Html->link(__('Login'), ['action' => 'login']) ?>
                <?= $this->Form->end() ?>
        </div>
    </div>
  </div>
<?php } ?>
<script>
  $(function(){
    $("#passwordRecovery").on("submit", function(e){
      var email = $("input[name=email]");
      if (email.val() == '') {
        email.addClass("alert-danger");
        e.preventDefault();
      }
    });
    $("#changePassword").on("click", function(e){
      var email = $("input[name=email]");
      var mode = <?= ($forPasswordChange) ? 1 : 0 ?>;
      var check = true;
      if (mode == 1) {
        var password = $("input[name=new_password]");
        var confirmPassword = $("input[name=confirm_new_password]");
        var verificationCode = $("input[name=verification_code]");
        if (password.val() == '') {
          password.addClass("alert-danger");
          check = false;
        } else {
          password.removeClass("alert-danger").addClass("alert-success");
        }
        if (confirmPassword.val() == '') {
          confirmPassword.addClass("alert-danger");
          check = false;
        } else {
          confirmPassword.removeClass("alert-danger").addClass("alert-success");
        }
        if (verificationCode.val() == '') {
          verificationCode.addClass("alert-danger");
          check = false;
        } else {
          verificationCode.removeClass("alert-danger").addClass("alert-success");
        }
      }
      if (email.val() == '') {
        email.addClass("alert-danger");
        check = false;
      } else {
        email.removeClass("alert-danger").addClass("alert-success");
      }
      if (!check) {
        e.preventDefault();
      }
    });
  });  
</script>