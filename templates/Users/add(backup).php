<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?= $this->Html->link(__('List Users'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
    <div class="column-responsive column-80">
        <div class="users form content">
            <?= $this->Form->create($user, ['type' => 'file', 'action' => 'add']) ?>
            <fieldset>
                <legend><?= __('Add User') ?></legend>
                <?php
                    echo $this->Form->control('user_name', ['value' => 'jptrans']);
                    echo $this->Form->control('full_name', ['value' => 'Jomar Transfiguracion']);
                    echo $this->Form->control('email', ['value' => 'aisheltrans11@gmail.com']);
                    echo $this->Form->control('photo', [
                        'type' => 'file',
                        'accept' => 'image/*'
                    ]);
                    echo $this->Form->control('password', ['value' => 'jptrans']);
                    //echo $this->Form->control('status');
                    //echo $this->Form->control('activation_code');
                    //echo $this->Form->control('date_activated', ['empty' => true]);
                    //echo $this->Form->control('last_login_time', ['empty' => true]);
                    //echo $this->Form->control('is_deleted');
                ?>
            </fieldset>
            <?= $this->Form->button(__('Submit')) ?>
            <?= $this->Form->end() ?>
        </div>
    </div>
</div>
