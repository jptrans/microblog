<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
?>
<div class="row">
  <div class="column-responsive column-80">
    <div class="users form content">
        <?= $this->Form->create($user, ['type' => 'file']) ?>
        <div class="row">
          <legend><?= __('Edit User') ?></legend>
            <div class="col-md-3">
                <?php 
                    echo $this->Html->image(
                        'photos/' . h($user->photo), array(
                            'alt' => 'MicroblogByJPT',
                            'border' => '0',
                            'data-src' => 'holder.js/100%x100',
                            'style' => ['max-height:150px;width:auto;height:auto;'
                        ])
                    );
                    echo $this->Form->control('change_photo', [
                        'name' => 'photo',
                        'id' => 'photo',
                        'type' => 'file',
                        'accept' => 'image/*',
                        'class' => 'form-control'
                    ]);
                ?>
            </div>
            <div class="col-md-7">
                <?php
                    echo $this->Form->control('user_name', [
                      'class' => 'form-control',
                      'required' => false,
                      'readonly'
                      ]
                    );
                    echo $this->Form->control('full_name', [
                      'class' => 'form-control',
                      'required' => false
                      ]
                    );
                    echo $this->Form->control('email', [
                      'class' => 'form-control',
                      'required' => false
                      ]
                    );
                    echo $this->Form->control('password', [
                      'class' => 'form-control',
                      'required' => false
                      ]
                    );
                    echo $this->Form->control('confirm_password', [
                      'type' => 'password',
                      'class' => 'form-control',
                      'value' => $user->password,
                      'required' => false
                      ]);
                    echo $this->Form->button('Submit', ['class' => 'btn btn-primary mt-1']);
                    echo $this->Form->end();
                ?>
            </div>
            <div class="col-md-1" style="text-right">
              <?= $this->Form->postLink(__('Deactivate'),
                  ['action' => 'deactivate', $user->id],
                  ['confirm' => __('Are you sure you want to deactivate?')])
              ?>
            </div>
        </div>
    </div>
  </div>
</div>

<script>
  $(function(){
    $("form").on("submit", function(e){
      var userName = $("input[name=user_name]");
      var fullName = $("input[name=full_name]");
      var email = $("input[name=email]");
      var password = $("input[name=password]");
      var confirmPassword = $("input[name=confirm_password]");
      var check = true;
      if (userName.val() == '') {
        userName.addClass("alert-danger");
        check = false;
      } else {
        userName.removeClass("alert-danger").addClass("alert-success");
      }
      if (fullName.val() == '') {
        fullName.addClass("alert-danger");
        check = false;
      } else {
        fullName.removeClass("alert-danger").addClass("alert-success");
      }
      if (email.val() == '') {
        email.addClass("alert-danger");
        check = false;
      } else {
        email.removeClass("alert-danger").addClass("alert-success");
      }
      if (password.val() == '') {
        password.addClass("alert-danger");
        check = false;
      } else {
        password.removeClass("alert-danger").addClass("alert-success");
      }
      if (confirmPassword.val() == '') {
        confirmPassword.addClass("alert-danger");
        check = false;
      } else {
        confirmPassword.removeClass("alert-danger").addClass("alert-success");
      }
      if (!check) {
        e.preventDefault();
      }
    });
  });
</script>