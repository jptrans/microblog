<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User $user
 */
$loggedUserId = $this->Identity->get('id');
?>

<div class="row view content">
 <div class="col-md-3 photoImg">
    <?php 
        echo $this->Html->image(
            'photos/' . h($user->photo), array(
                'alt' => 'MicroblogByJPT',
                'border' => '0',
                'data-src' => 'holder.js/100%x100',
                'style' => ['max-height:150px;max-width:250px;width:auto;height:auto;']
            )
        );
    ?>
  </div>
  <div class="col-md-6">
    <div class="row">
      <div class="col">
        <h2><?= utf8_decode($user->full_name) ?></h2>
      </div>
    </div>
    <div class="row">
      <div class="col">
        <?= h($user->email) ?>
      </div>
    </div>
    <div class="row">
      <div class="col">
        Member since 
        <?= h(date('F j, Y', strtotime($user->created))) ?>
      </div>
    </div>
  </div>
  <div class="col-md-3">
      <?php if ($loggedUserId === $user->id) { ?>
        <div class="row">
            <?php
                if ($this->Number->format($user->status) === '1') {
                    echo $this->Form->postLink(__('Deactivate'),[
                            'action' => 'deactivate',
                            $user->id
                        ],[
                            'confirm' => __('Are you sure you want to deactivate?')
                        ]
                    );
                }
            ?>
        </div>
        <div class="row">
            <?= $this->Html->link(__('Edit'), ['action' => 'edit', $user->id]) ?>
        </div>
      <?php } else { 
                if (isset($follow->id)) {
                    if (!$follow->is_deleted) {
                        echo $this->Html->link(__('Unfollow'), [
                            'controller' => 'follows',
                            'action' => 'edit',
                            $follow->id,
                            1, 0
                            ]);
                        if ($follow->is_favourite) {
                            echo '/' . $this->Html->link(__('Unfavourite'), [
                                'controller' => 'follows',
                                'action' => 'edit',
                                $follow->id,
                                0, 0
                            ]);
                        } else {
                            echo '/' . $this->Html->link(__('Favourite'), [
                                'controller' => 'follows',
                                'action' => 'edit',
                                $follow->id,
                                0, 1
                            ]);
                        }
                    } else {
                      echo $this->Html->link(__('Follow'), [
                          'controller' => 'follows',
                          'action' => 'edit',
                          $follow->id,
                          0
                      ]);
                    }
                } else {
                    echo $this->Html->link(__('Follow'), [
                        'controller' => 'follows',
                        'action' => 'add',
                        $user->id
                    ]);
                }        
      ?>
      <?php } ?>
  </div>
</div>

<div class="row content">
  <table class="table table-bordered">
    <th>Followings</th>
    <th>Followers</th>
    <tbody style="text-align:center">
      <td><?= $results['total_followings'] ?></td>
      <td><?= $results['total_followers'] ?></td>
    </tbody>
  </table>
</div>