
<div class="row">
  <div class="column-responsive d-flex justify-content-center">
    <div class="users form content">
        <?= $this->Flash->render() ?>
        <h3>Login to MicroblogByJPT</h3>
        <?= $this->Form->create() ?>
        <fieldset>
          <legend><?= __('Please enter your username and password') ?></legend>
          <?= $this->Form->control('user_name', ['class'=>'form-control']) ?>
          <?= $this->Form->control('password', ['class'=>'form-control']) ?>        
        </fieldset>
        <?= $this->Form->button(__('Submit'), ['class' => 'btn btn-primary mt-1']) ?>
        <?= $this->Html->link(__('Sign Up'), ['action' => 'add']) . ' |' ?>
        <?= $this->Html->link(__('Forgot password?'), ['action' => 'forgotPassword']) ?>
        <?= $this->Form->end() ?>
    </div>
  </div>
</div>
<script>
  $(function(){
    $("form").on("submit", function(e){
      var userName = $("input[name=user_name]");
      var password = $("input[name=password]");
      var check = true;

      if (userName.val() == '') {
        userName.addClass("alert-danger");
        check = false;
      } else {
        userName.removeClass("alert-danger").addClass("alert-success");
      }
      if (password.val() == '') {
        password.addClass("alert-danger");
        check = false;
      } else {
        password.removeClass("alert-danger").addClass("alert-success");
      }
      if (!check) {
        e.preventDefault();
      }
    });
  });
</script>