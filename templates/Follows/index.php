<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Follow[]|\Cake\Collection\CollectionInterface $follows
 */
?>
<div class="follows index content">
  <div class="row"  style="text-align:center">
    <div class="col-md-6" style="max-height:400px;overflow-y:scroll">
      <h2>FOLLOWING</h2>
          <?php if (count($followings) > 0) { ?>
              <?php foreach ($followings as $following) : ?>
                  <div class="row" style="text-align:left;margin-top:50px">
                    <div class="col-md-2">
                      <a href="<?= $this->Url->build([
                          'controller' => 'users',
                          'action' => 'view',
                          $following->user->id
                          ]) ?>">
                          <?php 
                              echo $this->Html->image(
                                  'photos/' . $following->user->photo, array(
                                      'alt' => 'MicroblogByJPT',
                                      'border' => '0',
                                      'data-src' => 'holder.js/100%x100',
                                      'style' => ['max-width:50px;max-height:50px;width:auto;height:auto']
                                  )
                              );
                          ?>
                      </a>
                    </div>
                    <div class="col-md-6">
                      <?= $this->Html->link(__($following->user->full_name), [
                          'action' => 'view',
                          'controller' => 'users',
                          $following->user->id]) ?>
                    </div>
                    <div class="col-md-4">
                      <?php
                          if (!$following->is_deleted) {
                              echo $this->Html->link(__('Unfollow'), [
                                  'controller' => 'follows',
                                  'action' => 'edit',
                                  $following->id,
                                  1, 0
                              ]);
                              if ($following->is_favourite) {
                                  echo '/' . $this->Html->link(__('Unfavourite'), [
                                      'controller' => 'follows',
                                      'action' => 'edit',
                                      $following->id,
                                      0, 0
                                  ]);
                              } else {
                                  echo '/' . $this->Html->link(__('Favourite'), [
                                      'controller' => 'follows',
                                      'action' => 'edit',
                                      $following->id,
                                      0, 1
                                  ]);

                              }
                          } else {
                              echo $this->Html->link(__('Follow'), [
                                  'controller' => 'follows',
                                  'action' => 'edit',
                                  $following->id,
                                  0
                              ]);
                          }
                      ?>
                    </div>
                  </div>
              <?php endforeach ?>
          <?php } else { echo 'No following yet.'; } ?>
    </div>
    <div class="col-md-6" style="max-height:600px;overflow-y:scroll">
      <h2>FOLLOWER</h2>
        <?php if (count($followers) > 0) { ?>
            <?php foreach ($followers as $index => $follower) : ?>
            <div class="row" style="text-align:left;margin-top:50px">
              <div class="col-md-2">
                <a href="<?= $this->Url->build([
                    'controller' => 'users',
                    'action' => 'view',
                    $follower->id])?>">
                    <?php 
                        echo $this->Html->image(
                            'photos/' . $follower->photo, array(
                                'alt' => 'MicroblogByJPT',
                                'border' => '0',
                                'data-src' => 'holder.js/100%x100',
                                'style' => ['max-width:50px;max-height:50px;width:auto;height:auto']
                            )
                        );
                    ?>
                </a>
              </div>
              <div class="col-md-10">
                <?= $this->Html->link(__($follower->full_name), [
                    'action' => 'view',
                    'controller' => 'users',
                    $follower->id]) ?>
              </div>
            </div>
            <?php endforeach ?>
        <?php } else { echo 'No follower yet.'; } ?>
    </div>
  </div>
</div>
