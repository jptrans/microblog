<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post $post
 */
?>
<div class="row">
    <aside class="column">
        <div class="side-nav">
            <h4 class="heading"><?= __('Actions') ?></h4>
            <?php if ($post->user_id === $this->Identity->get('id')) : ?>
            <?= $this->Html->link(__('Edit Post'), ['action' => 'edit', $post->id], ['class' => 'side-nav-item']) ?>
            <?= $this->Form->postLink(__('Delete Post'), ['action' => 'softDelete', $post->id], ['confirm' => __('Are you sure you want to delete # {0}?', $post->id), 'class' => 'side-nav-item']) ?>
            <?php endif ?>
            <?= $this->Html->link(__('Back to home'), ['action' => 'index'], ['class' => 'side-nav-item']) ?>
        </div>
    </aside>
</div>

<div class="row" style="margin-bottom:50px">
  <div class="col-md-8">
    <div class="row">
      <div class="col-md-1">
        <?php 
            echo $this->Html->image(
                'photos/' . $post->user->photo, array(
                    'alt' => 'MicroblogByJPT',
                    'border' => '0',
                    'data-src' => 'holder.js/100%x100'
                )
            );
        ?>
      </div>
      <div class="col-md-7">
        <div class="row">
            <?= $post->user->full_name ?>
        </div>
      </div>
      <div class="col-md-3">
      <?= $post->created ?>
      </div>
      <div class="col-md-1">
          <?= $this->Html->link(__('View'), ['action' => 'view', $post->id], ['class' => 'side-nav-item'])?>
          <br>
      <?php
          if ($this->Identity->get('id') === $post->user_id) {
            echo $this->Html->link(__('Edit'), ['action' => 'edit', $post->id], ['class' => 'side-nav-item']);
            echo '<br>';
            echo $this->Form->postLink(
              __('Delete'),
              ['action' => 'softDelete', $post->id],
              ['confirm' => __('Are you sure you want to delete # {0}?', $post->id), 'class' => 'side-nav-item']);
          }
          
      ?>
      </div>
    </div>
    <div class="row">
      <div class="col-md-1"></div>
      <div class="col-md-11">
        <p><?= h($post->body) ?></p>
      </div>
    </div>
    <?php if ($post->image !== null) : ?>
    <div class="row">
      <div class="col-md-12">
        <center>
        <?php 
            echo $this->Html->image(
                'posts/' . $post->image, array(
                    'alt' => 'MicroblogByJPT',
                    'border' => '0',
                    'data-src' => 'holder.js/100%x100',
                    'style' => ['max-height:150px;width:auto;height:auto;']
                )
            );
        ?>
        </center>
      </div>
    </div>
    <?php endif ?>
    <div class="row">
      <div class="col-md-3 text-left">
        Like
      </div>
      <div class="col-md-6 text-center">
        Comment
      </div>
      <div class="col-md-3 text-right">
        Share
      </div>
    </div>
  </div>
  <div class="col-md-4">
  </div>
</div>