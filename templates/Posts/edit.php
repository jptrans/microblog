<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Post $post
 */
?>
<div class="row">
  <aside class="column">
    <div class="side-nav">
      <h4 class="heading"><?= __('Actions') ?></h4>
        <?= $this->Html->link(__('Cancel'), ['action' => 'index']) ?>
        <?= $this->Form->postLink(
            __('Delete'),
            ['action' => 'delete', $post->id],
            ['confirm' => __('Are you sure you want to delete # {0}?', $post->id)]) ?>
        <?= $this->Html->link(__('View Post'), ['action' => 'view', $post->id]) ?>
      </div>
  </aside>
  <div class="column-responsive column-80">
    <div class="posts form content">
      <?= $this->Form->create($post, ['type' => 'file']) ?>
        <div class="row">
            <?php
                echo $this->Form->control('body', [
                    'name' => 'body',
                    'label' => '',
                    'id' => 'body',
                    'type' => 'textarea',
                    'rows' => 3,
                    'placeholder' => 'Show us what you have....',
                    'style' => ['width:100%;resize:none'],
                    'class' => 'form-control mb-2'
                ]);
            ?>
        </div>
 
        <?php if ($post->image !== null) : ?>
        <div class="row">
          <center>
            <?php 
                echo $this->Html->image(
                    'posts/' . $post->image, array(
                        'alt' => 'MicroblogByJPT',
                        'border' => '0',
                        'data-src' => 'holder.js/100%x100',
                        'style' => ['max-height:150px;width:auto;height:auto']
                    )
                );
            ?>
          </center>
        </div>
        <?php endif ?>
 
        <div class="row">
          <div class="col-md-3">
            <?php
                //echo $this->Form->control('image', [
                echo $this->Form->control(' ', [
                    'name' => 'photo',
                    'id' => 'photo',
                    'type' => 'file',
                    'accept' => 'image/*',
                    'class' => 'form-control'
                    //'style' => ['display:none']
                ]);
            ?>
          </div>
          <div class="col-md-9">
            <?= $this->Form->button('Update Post', [
                'class' => 'btn btn-secondary mt-4 form-control',
                'id' => 'submitPost'
                ]
            ) ?>
          </div>
          <?= $this->Form->end() ?>
        </div>
    </div>
  </div>
</div>
<script>
  $("#body").keydown(function(event) {
    if (event.ctrlKey && event.keyCode == 13) {
        $("#submitPost").click();
    }
  });
</script>