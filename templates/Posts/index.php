<?php
    function timeDiff($postCreation){
        $difference = strtotime('now') - strtotime($postCreation);
        $min = 60;
        $hour = 3600;
        $day = 86400;
        if (
            $difference > $day 
            && $difference < $day * 7
        ) {
            $time = intVal($difference / $day);
            $timeValue = ($time == 1) ? 'day' : 'days';
            return $time . ' ' . $timeValue . ' ago.';
            
        } elseif (
            $difference > $hour
            && $difference < $hour * 24
        ) {
            $time = intVal($difference / $hour);
            $timeValue = ($time == 1) ? 'hour' : 'hours';
            return $time . ' ' . $timeValue . ' ago.';
        } elseif (
            $difference > $min
            && $difference < $min * 60
        ) {
            $time = intVal($difference / $min);
            $timeValue = ($time == 1) ? 'min' : 'mins';
            return $time . ' ' . $timeValue . ' ago.';
        } elseif (
            $difference < 60
        ) {
            return 'Just now.';
        }
        return date('M j, Y', strtotime($postCreation));
    }
?>
<div class="row">
  <div class="col-md-12">
    <div class="column-responsive column-80">
      <div class="posts form content">
        <?php
            echo $this->Form->create($posts,['type' => 'file',
                'action' => 'addPost',
                'id' => 'addPost'
            ]);
        ?>
        <div class="row">
            <?php
                echo $this->Form->control('', [
                    'name' => 'body',
                    'id' => 'body',
                    'type' => 'textarea',
                    'rows' => 3,
                    'placeholder' => 'Show us what you have....',
                    'style' => ['width:100%;resize:none'],
                    'class' => 'form-control'
                ]);
            ?>
        </div>
        <div class="row">
          <div class="col-md-3">
            <?php
                echo $this->Form->control('', [
                    'name' => 'photo',
                    'id' => 'photo',
                    'type' => 'file',
                    'accept' => 'image/*',
                    'class' => 'form-control'
                ]);
            ?>
          </div>
          <div class="col-md-9">
            <?php
                echo $this->Form->button('Post', [
                    'type' => 'submit',
                    'id' => 'submitPost',
                    'class' => 'btn btn-secondary mt-4 form-control'
                ]);
            ?>
            <?= $this->Form->end() ?>
          </div>
        </div>
      </div>
    </div>
  </div>
<div>


<?php foreach ($posts as $index => $post): ?>

<?php
    $postImage = $post->image;
    $postBody = $post->body;
    $postIsPosted = $post->is_posted;
    $pkPostIsPresent = false;
    $postId = $post->id;
    $postCreated = $post->created;

    if ($post->repost_user_id !== null) {
        foreach ($pk_posts as $pk_post) {
            if ($pk_post->id === $post->repost_id) {
                $postImage = $pk_post->image;
                $postBody = $pk_post->body;
                $postId = $pk_post->id;
                $pkPostIsPresent = true;
                $postCreated = $pk_post->created;
                break;
            }
        }
    }
?>
<div class="content mt-2 w-90">
  <div class="row">
    <div class="col-md-12">
        <?php if ($post->repost_user_id !== null && $postIsPosted) : ?>
          <div class="row">
            <div class="row">
              <div class="col-md-1">
                  <?php
                      foreach ($users as $user) {
                          if ($user->id === $post->repost_user_id) {
                              break;
                          }
                      }
                  ?>
                  <a href="<?= $this->Url->build(['controller' => 'users', 'action' => 'view', $user->id]) ?>">
                      <?php 
                          echo $this->Html->image(
                              'photos/' . $user->photo, [
                                  'alt' => 'MicroblogByJPT',
                                  'style' => 'max-width:50px;max-height:50px;width:auto;height:auto'
                              ]);
                      ?>
                  </a>
              </div>
              <div class="col-md-7">
                  <?php
                      echo $this->Html->link(__($user->full_name), [
                          'controller' => 'users',
                          'action' => 'view',
                          $user->id
                      ]);
                  ?>
              </div>
              <div class="col-md-3" style="text-align:right">
                  <?= timeDiff($post->created) ?>
              </div>
              <div class="col-md-1" style="text-align:right">
                <?php
                    echo $this->Html->link($this->Html->tag('i', '', [
                        'class' => 'bi bi-eye',
                        'data-toggle' => 'tooltip',
                        'title' => 'View Post'
                        ]), [
                            'action' => 'view',
                            $post->id
                        ], [
                            'escape' => false
                        ]
                    );
                    
                    if ($this->Identity->get('id') === $post->repost_user_id) {
                        echo '|' . $this->Html->link($this->Html->tag('i', '', [
                            'class' => 'bi bi-trash',
                            'data-toggle' => 'tooltip',
                            'title' => 'Delete Post'
                            ]), [
                                'action' => 'softDelete',
                                $post->id
                            ], [
                                'confirm' => __('Are you sure you want to delete your post?'),
                                'escape' => false
                            ]
                        );
                    }
                ?>
              </div>
            </div>
  
            <div class="row">
              <div class="col-md-1"></div>
              <div class="col-md-10">
        <?php endif ?>
  
                <div class="row">
                  <div class="col-md-1">
                    <a href="<?= $this->Url->build(['controller' => 'users', 'action' => 'view', $post->user->id]) ?>">
                        <?php 
                            echo $this->Html->image('photos/' . $post->user->photo, [
                                'alt' => 'MicroblogByJPT',
                                'style' => 'max-width:50px;max-height:50px;width:auto;height:auto'
                            ]);
                        ?>
                    </a>
                  </div>
                  <div class="col-md-7">
                      <?php
                          echo $this->Html->link(__($post->user->full_name), [
                              'controller' => 'users',
                              'action' => 'view',
                              $post->user_id
                          ]);
                      ?>
                  </div>
                  <div class="col-md-3" style="text-align:right">
                      <?= timeDiff($postCreated) ?>
                  </div>
                  <div class="col-md-1" style="text-align:right">
                    <?php
                        $viewPostId = $post->id;
                        if ($post->repost_user_id !== null) {
                            $viewPostId = $post->repost_id;
                        }
                        echo $this->Html->link($this->Html->tag('i', '', [
                            'class' => 'bi bi-eye',
                            'data-toggle' => 'tooltip',
                            'title' => 'View Post'
                            ]), [
                                'action' => 'view',
                                $viewPostId
                            ], [
                                'escape' => false
                            ]
                        );
                        
                        if (
                            $this->Identity->get('id') === $post->user_id
                            && $post->repost_user_id === null
                        ) {
                            
                            echo '|' . $this->Html->link($this->Html->tag('i', '', [
                                'class' => 'bi bi-pencil-square',
                                'data-toggle' => 'tooltip',
                                'title' => 'Edit Post'
                                ]), [
                                    'action' => 'edit',
                                    $post->id
                                ], [
                                    'escape' => false
                                ]
                            );                        
                            echo '|' . $this->Html->link($this->Html->tag('i', '', [
                                'class' => 'bi bi-trash',
                                'data-toggle' => 'tooltip',
                                'title' => 'Delete Post'
                                ]), [
                                    'action' => 'softDelete',
                                    $post->id
                                ], [
                                    'confirm' => __('Are you sure you want to delete your post?'),
                                    'escape' => false
                                ]
                            );
                        }
                    ?>
                  </div>
                </div>
  
        <?php if ($post->repost_user_id !== null) : ?>
              </div>
              <div class="col-md-1"></div>
            </div>
          </div>
        <?php endif ?>
  
      <div class="row">
        <div class="col-md-1"></div>
        <div class="col-md-10">
          <p><?= utf8_encode($postBody) ?></p>
        </div>
        <div class="col-md-1"></div>
      </div>
  
      <?php if ($postImage !== null) : ?>
        <div class="row mb-2">
          <div class="col-md-12 text-center">
                <?php 
                    echo $this->Html->image(
                        'posts/' . $postImage, array(
                            'alt' => 'MicroblogByJPT',
                            'border' => '0',
                            'data-src' => 'holder.js/100%x100',
                            'style' => ['max-height:150px;width:auto;height:auto;']
                        )
                    );
                ?>
          </div>
        </div>
      <?php endif ?>
  
      <div class="row">
        <hr>
        <div class="col-md-3">
            <?php
                $loggedUserId = $this->Identity->get('id');
                $likeIsDeleted = -1;
                $likeUserId = -1;
    
                if (isset($post->likes)) {
                    for ($i = 0; $i < count($post->likes); $i++) {
                        if ($loggedUserId == $post->likes[$i]['user_id']) {
                            $likeUserId = $post->likes[$i]['user_id'];
                            $likeId = $post->likes[$i]['id'];
                            $likeIsDeleted = $post->likes[$i]['is_deleted'];
                            break;
                        }
                    }
    
                    if (
                        $likeIsDeleted == 0
                        && $likeUserId == $loggedUserId
                        ) {
                            echo $this->Html->link($this->Html->tag('i', 'Unlike', [
                                'class' => 'bi bi-hand-thumbs-up-fill'
                                ]), [
                                    'controller' => 'likes',
                                    'action' => 'edit',
                                    $post->id, 0
                                ], [
                                    'escape' => false
                                ]
                            );
                    } else if 
                    (
                        $likeIsDeleted == 1
                        && $likeUserId == $loggedUserId
                    ) {
                        echo $this->Html->link($this->Html->tag('i', 'Like', [
                            'class' => 'bi bi-hand-thumbs-up'
                            ]), [
                                'controller' => 'likes',
                                'action' => 'edit',
                                $post->id, 0
                            ], [
                                'escape' => false
                            ]
                        );
                        
                    } else {
                        echo $this->Html->link($this->Html->tag('i', 'Like', [
                            'class' => 'bi bi-hand-thumbs-up'
                            ]), [
                                'controller' => 'likes',
                                'action' => 'add',
                                $post->id
                            ], [
                                'escape' => false
                            ]
                        );
                    }
    
                } else {
                    echo $this->Html->link(__('Like'), [
                        'controller' => 'likes',
                        'action' => 'add',
                        $post->id
                    ]);
                }
                
                $totalLikes = 0;
                foreach ($post->likes as $post_like) {
                    if (!$post_like->is_deleted) {
                        $totalLikes += 1;
                    }
                }
                
                if ($totalLikes > 0) {
                    echo ' (' . count($post->likes) . ')';
                } else {
                    echo ' Be the first to like.';
                }
            ?>
        </div>
          <?php
              $countComment = 0;
              foreach ($post->comments as $c) {
                  if ($c->is_deleted === false) {
                      $countComment++;
                  }
              }
          ?>
        <div class="col-md-6 text-center" id="divComment<?= $post->id ?>">
          <i class="bi bi-chat-left">
            <span onclick="comment(this.id)" style="cursor:pointer" id="<?= $post->id ?>">
              Comment <?= ($countComment == 0) ? '(Be the first to comment.)' : '(' . $countComment . ')' ?>
            </span>
          </i>
  
          <div class="row" style="display:none" id="divForm<?= $post->id ?>">
            <?php
                echo $this->Form->create($post, [
                        'action' => 'addComment',
                        'onSubmit' => 'return addComment(this.post_id.value)'
                    ]
                );
            ?>
                <?= $this->Form->control(' ',[
                    'name' => 'message',
                    'id' => 'message' . $post->id,
                    'class' => 'form-control'
                    ])
                ?>
                <?= $this->Form->hidden('post_id',['value' => $post->id]) ?>
                <?= $this->Form->hidden('user_id',['value' => $loggedUserId])?>
            <?= $this->Form->end() ?>
  
              <?php foreach ($post->comments as $postComment) : ?>
                  <?php if ($postComment->is_deleted === false) : ?>
                    <div class="row">
                      <div class="col-md-2">
                          <?php foreach ($users as $user) : ?>
                              <?php if ($user->id === $postComment->user_id) : ?>
                                <a href="users/view/<?= $postComment->user_id ?>" title="<?= $user->full_name ?>">
                                    <?php 
                                        echo $this->Html->image('photos/' . $user->photo, [
                                            'alt' => 'MicroblogByJPT',
                                            'style' => 'max-width:20px;max-height:20px;width:auto;height:auto'
                                        ]);
                                    ?>
                                </a>
                              <?php break;endif ?>
                          <?php endforeach ?>
                      </div>
                      <div class="col-md-8" style="text-align:left" title="<?= $postComment->modified ?>">
                          <?= utf8_encode($postComment->message) ?>
                      </div>
                      <div class="col-md-2">
                        <?php
                            if ($this->Identity->get('id') === $postComment->user_id) {
                                  
                                echo $this->Html->link($this->Html->tag('i', '', [
                                    'class' => 'bi bi-pencil-square',
                                    'data-toggle' => 'tooltip',
                                    'title' => 'Edit Comment'
                                    ]), [
                                        'controller' => 'comments',
                                        'action' => 'edit',
                                        $postComment->id
                                    ], [
                                        'escape' => false
                                    ]
                                );                        
                                echo '|' . $this->Html->link($this->Html->tag('i', '', [
                                    'class' => 'bi bi-trash',
                                    'data-toggle' => 'tooltip',
                                    'title' => 'Delete Comment'
                                    ]), [
                                        'controller' => 'comments',
                                        'action' => 'softDelete',
                                        $postComment->id
                                    ], [
                                        'confirm' => __(
                                            'Are you sure you want to delete your comment?'
                                        ),
                                        'escape' => false
                                    ]
                                );
                              }
                        ?>
                      </div>
                    </div>
                  <?php endif ?>
              <?php endforeach ?>
          </div>
        </div>
        <div class="col-md-3" style="text-align:right">
          <?php
              $countSharePosts = 0;
              foreach ($pk_posts as $countShare) {
                  if ($countShare->repost_id !== $post->id) {
                      continue;
                  }
                  $countSharePosts++;
              }
              if ($countSharePosts > 0) {
                  $countSharePosts = '(' . $countSharePosts . ')';
              } else {
                  $countSharePosts = '';
              }
          ?>
            <?php              
                echo $this->Html->link($this->Html->tag('i', 'Share', [
                    'class' => 'bi bi-box-arrow-up-right'
                    ]), [
                        'action' => 'addSharePost',
                        $postId,
                        $post->user_id
                    ], [
                        'escape' => false
                    ]
                ) . $countSharePosts;
            ?>
        </div>
      </div>
    </div>
  </div>
</div>
<?php endforeach ?>

<div class="paginator">
    <ul class="pagination">
        <?= $this->Paginator->first('<< ' . __('first')) ?>
        <?= $this->Paginator->prev('< ' . __('previous')) ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next(__('next') . ' >') ?>
        <?= $this->Paginator->last(__('last') . ' >>') ?>
    </ul>
    <p><?= $this->Paginator->counter(__('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')) ?></p>
</div>

<script>
  function comment(id = null) {
    var formId = document.getElementById("divForm"+id);

    if (formId.style.display === "none") {
      return formId.style.display = 'block';
    }
    return formId.style.display = 'none';
  }
  $("#body").keydown(function(event) {
    if (event.ctrlKey && event.keyCode == 13) {
        $("#submitPost").click();
    }
  });
  function addComment(id){
      var commentMessage = $("#message"+id);
      if (commentMessage.val() == '') {
        commentMessage.addClass("alert-danger");
        return false;
      }
  }
  $(function(){
    $("#addPost").on("submit", function(e){
      var postBody = $("textarea#body");
      var check = true;
      if (postBody.val() == '') {
        postBody.addClass("alert-danger");
        check = false;
      } else {
        postBody.removeClass("alert-danger").addClass("alert-success");
      }
      if (!check) {
        e.preventDefault();
      }
    });
  });
</script>
