CREATE TABLE users (
    id INT(20) AUTO_INCREMENT PRIMARY KEY,
    user_name VARCHAR(255) NOT NULL,
    full_name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    photo VARCHAR(255) NOT NULL,
    password VARCHAR(255) NOT NULL,
    created DATETIME NOT NULL,
    modified DATETIME,
    status INT(11) NOT NULL,
    activation_code VARCHAR(255) NOT NULL,
    date_activated DATETIME,
    last_login_time DATETIME,
    is_deleted TINYINT(1)
) ENGINE = INNODB;

CREATE TABLE posts (
    id INT(20) AUTO_INCREMENT PRIMARY KEY,
    user_id INT(20) NOT NULL,
    body VARCHAR(140) NOT NULL,
    image VARCHAR(255) NOT NULL,
    created DATETIME NOT NULL,
    modified DATETIME,
    is_posted TINYINT(1) NOT NULL,
    repost_user_id INT(20),
    is_deleted TINYINT(1) NOT NULL
) ENGINE = INNODB;

CREATE TABLE follows (
    id INT(20) AUTO_INCREMENT PRIMARY KEY,
    user_id INT(20) NOT NULL,
    follower_user_id INT(20) NOT NULL,
    is_favourite TINYINT(1) NOT NULL,
    created	DATETIME NOT NULL,
    modified DATETIME,
    is_deleted	TINYINT(1) NOT NULL
) ENGINE = INNODB;

CREATE TABLE likes (
    id INT(20) AUTO_INCREMENT PRIMARY KEY,
    post_id	INT(20) NOT NULL,
    user_id	INT(20) NOT NULL,
    created	DATETIME NOT NULL,
    modified DATETIME,
    is_deleted TINYINT(1) NOT NULL
) ENGINE = INNODB;

CREATE TABLE comments (
    id INT(20) AUTO_INCREMENT PRIMARY KEY,
    post_id	INT(20) NOT NULL,
    user_id	INT(20) NOT NULL,
    message VARCHAR(140) NOT NULL,
    created	DATETIME NOT NULL,
    modified DATETIME,
    is_deleted TINYINT(1) NOT NULL
) ENGINE = INNODB;