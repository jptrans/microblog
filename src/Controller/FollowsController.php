<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Follows Controller
 *
 * @property \App\Model\Table\FollowsTable $Follows
 * @method \App\Model\Entity\Follow[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class FollowsController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null|void Renders view
     */
    public function index()
    {
        
        $result = $this->Authentication->getResult();
        $loggedUser = $result->getData();

        $this->paginate = [
            'contain' => ['Users'],
            'order' => ['Follows.created' => 'desc']
        ];
        $followersData = $this->paginate($this->Follows
            ->find('all')
            ->where(['user_id' => $loggedUser['id'], 'Follows.is_deleted' => false]));

        $this->loadModel('Users');
        $followers = [];
        $followers_id = [];
        foreach ($followersData as $followerData) {
            $followers[] = $this->Users->find()
                ->where(['id' => $followerData->follower_user_id])->first();
            $followers_id[] = $followerData->id;
        }

        $followings = $this->paginate($this->Follows->find()
            ->where([
                'follower_user_id' => $loggedUser['id'],
                'Follows.is_deleted' => false
                ]));

        $this->set(compact('followers', 'followings', 'followers_id'));
    }
    
    public function view($id = null)
    {
        $follow = $this->Follows->get($id, [
            //'contain' => ['Users', 'FollowerUsers'],
            'contain' => ['Users'],
        ]);

        $this->set(compact('follow'));
    }
    
    public function add($followingUserId = null)
    {
        
        $result = $this->Authentication->getResult();
        $loggedUser = $result->getData();

        $follow = $this->Follows->newEmptyEntity();
        if ($followingUserId !== null && $loggedUser['id']) {
            $follow->is_favourite = 0;
            $follow->is_deleted = 0;
            $follow->user_id = $followingUserId;
            $follow->follower_user_id = $loggedUser['id'];

            if ($this->Follows->save($follow)) {
                $this->Flash->success(__('The follow has been saved.'));
                return $this->redirect($this->referer());
                return $this->redirect(['controller' => 'users', 'action' => 'view', $followingUserId]);
            }
            $this->Flash->error(__('The follow could not be saved. Please, try again.'));
        }
        return $this->redirect($this->referer());
        return $this->redirect(['controller' => 'follows', 'action' => 'index']);
        $users = $this->Follows->Users->find('list', ['limit' => 200]);
        
        $this->set(compact('follow', 'users'));
    }
    
    public function edit($id = null, $is_deleted = null, $is_favourite = null)
    {
        $follow = $this->Follows->get($id, [
            'contain' => [],
        ]);
        
        $result = $this->Authentication->getResult();
        $loggedUser = $result->getData();

        if ($id !== null && $is_deleted !== null && $loggedUser['id'] === $follow->follower_user_id) {
            $follow->is_deleted = $is_deleted;

            if ($is_favourite !== null) {
                $follow->is_favourite = $is_favourite;
            }

            if ($this->Follows->save($follow)) {
                $this->Flash->success(__('The follow has been saved.'));
                return $this->redirect($this->referer());
                return $this->redirect(['controller' => 'users', 'action' => 'view', $follow->user_id]);
            }
            return $this->redirect($this->referer());
            $this->Flash->error(__('The follow could not be saved. Please, try again.'));
        }
        $users = $this->Follows->Users->find('list', ['limit' => 200]);
        
        $this->set(compact('follow', 'users'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Follow id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $follow = $this->Follows->get($id);
        if ($this->Follows->delete($follow)) {
            $this->Flash->success(__('The follow has been deleted.'));
        } else {
            $this->Flash->error(__('The follow could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
