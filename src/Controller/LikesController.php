<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Likes Controller
 *
 * @property \App\Model\Table\LikesTable $Likes
 * @method \App\Model\Entity\Like[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class LikesController extends AppController
{
    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add($post_id = null)
    {
        $like = $this->Likes->newEmptyEntity();


        $result = $this->Authentication->getResult();
        $logged_user = $result->getData();

        if ($post_id !== null && $logged_user['id']) {
            $like->is_deleted = 0;
            $like->post_id = $post_id;
            $like->user_id = $logged_user['id'];

            if ($this->Likes->save($like)) {
                $this->Flash->success(__('The like has been saved.'));

                return $this->redirect($this->referer());
            }
        }

        $this->Flash->error(__('The like could not be saved. Please, try again.'));
        return $this->redirect($this->referer());
    }

    /**
     * Edit method
     *
     * @param string|null $id Like id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null, $is_deleted = null)
    {
        $like = $this->Likes->get($id, [
            'contain' => [],
        ]);
        
        $result = $this->Authentication->getResult();
        $loggedUser = $result->getData();

        if ($id !== null && $is_deleted !== null && $loggedUser['id'] === $like->user_id) {
            $like->is_deleted = $is_deleted;
            if ($this->Likes->save($like)) {
                $this->Flash->success(__('The like has been saved.'));

                return $this->redirect($this->referer());
            }
        }
        
        $this->Flash->error(__('The like could not be saved. Please, try again.'));
        return $this->redirect($this->referer());

        $posts = $this->Likes->Posts->find('list', ['limit' => 200]);
        $users = $this->Likes->Users->find('list', ['limit' => 200]);
        $this->set(compact('like', 'posts', 'users'));
    }
}
