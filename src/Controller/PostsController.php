<?php

declare(strict_types=1);

namespace App\Controller;

class PostsController extends AppController
{
    
    public function index()
    {
        $result = $this->Authentication->getResult();
        $loggedUser = $result->getData();

        $this->paginate = [
            //'contain' => ['Users', 'RepostUsers'],
            'contain' => ['Users', 'Likes', 'Comments'],
            'conditions' => [
                'Posts.is_posted' => true,
                'Posts.is_deleted' => false
            ],
            'limit' => 10,
            'order' => [
                'Posts.created' => 'desc'
            ]
        ];
        
        $posts = $this->Posts->find()
            ->where([
                'Posts.is_posted' => true,
                'Posts.is_deleted' => false
                ])
            ->group(['Posts.id'])
            ->enableAutoFields(true);

        $this->paginate($posts);
        
        $this->loadModel('Users');
        $users = $this->Users->find()
            ->where([
                'is_deleted' => false
            ])
            ->all();

        $pk_posts = $this->Posts->find()->all();

        $this->set(compact('posts', 'users', 'pk_posts'));
    }

    public function loggedUser()
    {
        $result = $this->Authentication->getResult();
        return $result->getData();
    }
    
    public function myPosts()
    {
        $loggedUserId = $this->loggedUser()->id;

        $result = $this->Authentication->getResult();
        $loggedUser = $result->getData();

        $this->paginate = [
            //'contain' => ['Users', 'RepostUsers'],
            'contain' => ['Users', 'Likes', 'Comments'],
            'conditions' => [
                'Posts.is_posted' => true,
                'Posts.is_deleted' => false,
                'OR' => [
                    'Posts.user_id' => $loggedUserId,
                    'Posts.repost_user_id' => $loggedUserId,
                ]
            ],
            'limit' => 10,
            'order' => [
                'Posts.created' => 'desc'
            ]
        ];
        
        $posts = $this->Posts->find()
            ->where([
                'Posts.is_posted' => true,
                'Posts.is_deleted' => false,
                'OR' => [
                    'Posts.user_id' => $loggedUserId,
                    'Posts.repost_user_id' => $loggedUserId,
                ]
            ])
            ->group(['Posts.id'])
            ->enableAutoFields(true);

        $this->paginate($posts);
        
        $this->loadModel('Users');
        $users = $this->Users->find()
            ->where([
                'is_deleted' => false
            ])
            ->all();

        $pk_posts = $this->Posts->find()->all();

        $this->set(compact('posts', 'users', 'pk_posts'));
    }
    
    public function view($id = null)
    {
        $result = $this->Authentication->getResult();
        $loggedUser = $result->getData();

        $this->paginate = [
            //'contain' => ['Users', 'RepostUsers'],
            'contain' => ['Users', 'Likes', 'Comments'],
            'conditions' => [
                'Posts.id' => $id
            ]
        ];
        
        $posts = $this->Posts->find()
            ->where([
                'Posts.id' => $id
            ])
            ->group(['Posts.id'])
            ->enableAutoFields(true);

        $this->paginate($posts);
        
        $this->loadModel('Users');
        $users = $this->Users->find()
        ->where([
            'is_deleted' => false
        ])
        ->all();

        $pk_posts = $this->Posts->find()->all();
        foreach ($posts as $post) {
            if ($post->is_deleted) {
                $this->Flash->error(__('The post is not available.'));
                return $this->redirect($this->referer());
            }
        }

        $this->set(compact('posts', 'users', 'pk_posts'));
    }
    
    public function add()
    {
        $post = $this->Posts->newEmptyEntity();
        if ($this->request->is('put')) {
            if (empty($this->request->getData('body'))) {
                
                $this->Flash->error(__('The post must not be blank.'));

                return $this->redirect($this->referer());
            }
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            
            $post->is_posted = 1;
            $post->is_deleted = 0;
            $loggedUserId = $this->loggedUser()->id;
            $post->user_id = $loggedUserId;

            $post->body = htmlentities($this->request->getData('body'));
            $post->image = null;
            $postPhoto = $this->request->getData('photo');
            $name = $postPhoto->getClientFilename();
            $type = $postPhoto->getClientMediaType();
            $uniqPhotoName = uniqid() . '-' . $name;
            $targetPath = WWW_ROOT. 'img'. DS . 'posts'. DS. $uniqPhotoName;
            $isPhotoSaved = 'We only allowed jpeg, jpg and png.';
            if (
                $type == 'image/jpeg' 
                || $type == 'image/jpg' 
                || $type == 'image/png'
            ) {
                if (!empty($name)) {
                    if (
                        $postPhoto->getSize() > 0 
                        && $postPhoto->getError() == 0
                    ) {
                        $postPhoto->moveTo($targetPath); 
                        $post->image = $uniqPhotoName;
                        $isPhotoSaved = true;
                    }
                }
            }
            
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('The post has been saved.'));
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The post could not be saved. Please, try again.'));
        }

        $users = $this->Posts->Users->find('list', ['limit' => 200]);
        $this->set(compact('post', 'users'));
    }

    public function addSharePost($postId = null, $postUserId = null)
    {
        $post = $this->Posts->newEmptyEntity();

        $loggedUserId = $this->loggedUser()->id;

        if ($postId && $postUserId && $loggedUserId) {
            $post->user_id = $postUserId;
            $post->repost_id = $postId;
            $post->repost_user_id = $loggedUserId;
            $post->is_posted = 1;
            $post->is_deleted = 0;
            $post->body = '';
            
            if ($this->Posts->save($post)) {
                $this->Flash->success(__('The post has been reposted.'));
                return $this->redirect($this->referer());
            }
        }
        $this->Flash->error(__('The post could not be repost. Please, try again.'));
        return $this->redirect($this->referer());
    }
    
    public function edit($id = null)
    {
        $post = $this->Posts->get($id, [
            'contain' => [],
        ]);
        
        $loggedUserId = $this->loggedUser()->id;
        if ($post->user_id !== $loggedUserId) {
            return $this->redirect($this->referer());
        }
        if ($post->body == '') {
            return $this->redirect($this->referer());
        }

        if ($this->request->is('put')) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            $post->body = htmlentities($this->request->getData('body'));
            $postPhoto = $this->request->getData('photo');
            $name = $postPhoto->getClientFilename();
            $type = $postPhoto->getClientMediaType();
            $uniqPhotoName = uniqid() . '-' . $name;
            $targetPath = WWW_ROOT. 'img'. DS . 'posts'. DS. $uniqPhotoName;
            $isPhotoSaved = 'We only allowed jpeg, jpg and png.';
            if ($type == 'image/jpeg' || $type == 'image/jpg' || $type == 'image/png') {
                if (!empty($name)) {
                    if ($postPhoto->getSize() > 0 && $postPhoto->getError() == 0) {
                        $postPhoto->moveTo($targetPath); 
                        $post->image = $uniqPhotoName;
                        $isPhotoSaved = true;
                    }
                }
            }

            if ($this->Posts->save($post)) {
                $this->Flash->success(__('The post has been updated.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The post could not be saved. Please, try again.'));
            return $this->redirect($this->referer());
        }

        $users = $this->Posts->Users->find('list', ['limit' => 200]);
        $this->set(compact('post', 'users'));
    }

    public function softDelete($id = null)
    {
        $post = $this->Posts->get($id);
        
        $loggedUserId = $this->loggedUser()->id;
        if ($post->user_id !== $loggedUserId && $post->repost_user_id !== $loggedUserId) {
            $this->Flash->error(__('The post could not be deleted. Please, try again.'));
            return $this->redirect($this->referer());
        }

        if ($this->request->is(['get'])) {
            $post = $this->Posts->patchEntity($post, $this->request->getData());
            $post->is_posted = 0;
            $post->is_deleted = 1;

            if ($this->Posts->save($post)) {                

                $this->Flash->success(__('The post has been deleted.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The post could not be deleted. Please, try again.'));
            return $this->redirect($this->referer());
        }
        $this->Flash->error(__('The post could not be deleted. Please, try again.'));
        return $this->redirect($this->referer());
        $users = $this->Posts->Users->find('list', ['limit' => 200]);
        $this->set(compact('post', 'users'));
    }
}