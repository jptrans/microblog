<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Event\EventInterface;
/**
 * Likes Controller
 *
 * @property \App\Model\Table\LikesTable $Likes
 * @method \App\Model\Entity\Like[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SearchesController extends AppController
{

    public function initialize(): void
    {
        parent::initialize();

        $this->loadComponent('Search.Search', [
            'actions' => ['search'],
        ]);
    }

    public function bootstrap(): void
    {
        $this->addPlugin('Search');
    }
    
    public function search()
    {
        $this->loadModel('Users');
        $this->loadModel('Posts');

        $users = $this->paginate($this->Users->find('search', ['search' => $this->request->getQuery()])
        ->where([
            'status' => 1,
            'is_deleted' => false
            ]
        ));
        $posts = $this->paginate($this->Posts->find('search', ['search' => $this->request->getQuery()])
        ->where([
            'is_posted' => true,
            'is_deleted' => false
            ]
        ));

        $this->set(compact('users', 'posts'));
        $this->set('_serialize', ['users', 'posts']);
    }
}
