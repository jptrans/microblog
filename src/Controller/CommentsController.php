<?php
declare(strict_types=1);

namespace App\Controller;

/**
 * Comments Controller
 *
 * @property \App\Model\Table\CommentsTable $Comments
 * @method \App\Model\Entity\Comment[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class CommentsController extends AppController
{
    /**
     * View method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null|void Renders view
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $comment = $this->Comments->get($id, [
            'contain' => ['Posts', 'Users'],
        ]);

        $this->set(compact('comment'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $comment = $this->Comments->newEmptyEntity();

        if ($this->request->is('put')) {
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());

            $result = $this->Authentication->getResult();
            $user = $result->getData();
            if ($comment->user_id !== $user['id']) {
                return $this->redirect(['controller' => 'posts', 'action' => 'index']);
            }
            $comment->message = htmlentities($this->request->getData('message'));
            $comment->is_deleted = 0;
            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('The comment has been saved.'));
                return $this->redirect($this->referer());
                //return $this->redirect(['controller' => 'posts', 'action' => 'index']);
            }
            $this->Flash->error(__('The comment could not be saved. Please, try again.'));
        }
        $posts = $this->Comments->Posts->find('list', ['limit' => 200]);
        $users = $this->Comments->Users->find('list', ['limit' => 200]);
        $this->set(compact('comment', 'posts', 'users'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $comment = $this->Comments->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is('put')) {
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());

            $result = $this->Authentication->getResult();
            $user = $result->getData();
            if ($comment->user_id !== $user['id']) {
                return $this->redirect(['controller' => 'posts', 'action' => 'index']);
            }

            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('The comment has been saved.'));

                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The comment could not be saved. Please, try again.'));
        }
        $posts = $this->Comments->Posts->find('list', ['limit' => 200]);
        $users = $this->Comments->Users->find('list', ['limit' => 200]);
        $this->set(compact('comment', 'posts', 'users'));
    }

    /**
     * Soft Delete method
     *
     * @param string|null $id Comment id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function softDelete($id = null)
    {
        $comment = $this->Comments->get($id, [
            'contain' => [],
        ]);
        if ($this->request->is(['get'])) {
            $comment = $this->Comments->patchEntity($comment, $this->request->getData());

            $result = $this->Authentication->getResult();
            $user = $result->getData();
            if ($comment->user_id !== $user['id']) {
                return $this->redirect(['controller' => 'posts', 'action' => 'index']);
            }
            $comment->is_deleted = 1;
            if ($this->Comments->save($comment)) {
                $this->Flash->success(__('The comment has been deleted.'));
                return $this->redirect($this->referer());
            }
            $this->Flash->error(__('The comment could not be deleted. Please, try again.'));
        }
        $posts = $this->Comments->Posts->find('list', ['limit' => 200]);
        $users = $this->Comments->Users->find('list', ['limit' => 200]);
        $this->set(compact('comment', 'posts', 'users'));
    }
}
