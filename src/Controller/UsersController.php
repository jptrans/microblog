<?php
declare(strict_types=1);

namespace App\Controller;

use Cake\Mailer\Mailer;
use Cake\I18n\Time;


class UsersController extends AppController
{
    public function beforeFilter(\Cake\Event\EventInterface $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->addUnauthenticatedActions(['login', 'add', 'validate','forgotPassword']);
    }

    public function login()
    {   
        $this->request->allowMethod(['get', 'post']);
        $result = $this->Authentication->getResult();

        if ($result->isValid()) {

            $user = $result->getData();
            $id = $user['id'];
            $user = $this->Users->get($id);

            //For validation
            if ($user->status === 0) {
                $this->Authentication->logout();

                $this->Flash->success(__('Your account is not yet validated. Please validate now to start using MicroblogByJPT'));

                return $this->redirect(['controller'=>'Users','action' => 'validate', $id]);
            }

            //For reactivation
            if ($user->status === 2) {
                $this->Authentication->logout();

                $this->Flash->success(__('Your account is deactivated. Reactivate it now to start using MicroblogByJPT'));

                return $this->redirect(['controller'=>'Users','action' => 'validate', $id]);
            }
            
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->last_login_time = date('Y-m-d H:i:s');

            if (!$this->Users->save($user)) {
                $this->Flash->error(__('User last login time not updated.'));
            }

            return $this->redirect(['controller' => 'Posts', 'action' => 'index']);
        }

        if ($this->request->is('post') && !$result->isValid()) {
            $this->Flash->error(__('Invalid username or password'));
            
        }
        
    }

    public function logout()
    {
        $user = $this->Authentication->getResult();

        if ($user->isValid()) {
            $this->Authentication->logout();
            return $this->redirect(['controller' => 'Users', 'action' => 'login']);
        }
    }
    
    public function loggedUser()
    {
        $result = $this->Authentication->getResult();
        return $result->getData();
    }

    public function view($id = null)
    {
        $user = $this->Users->get($id, [
            'contain' => ['Comments', 'Follows', 'Likes', 'Posts'],
        ]);
        
        $loggedUserId = $this->loggedUser()->id;
        $this->loadModel('Follows');
        $this->loadModel('Posts');
        $this->loadModel('Likes');
        $this->loadModel('Comments');

        if ($loggedUserId !== intval($id)) {

            $follow = $this->Follows->find('all')
                ->where([
                    'follower_user_id' => $loggedUserId,
                    'user_id' => $id
                ])
                ->first();
            $this->set(compact('follow'));
        }

        $total_followers = $this->Follows->find()
            ->where([
                'is_deleted' => false,
                'user_id' => $id
            ])
            ->count();
        $total_followings = $this->Follows->find()
            ->where([
                'is_deleted' => false,
                'follower_user_id' => $id
            ])
            ->count();        
        $results = [
            'total_followings' => $total_followings,
            'total_followers' => $total_followers
        ];

        $this->set(compact('user','results'));
    }

    public function generateCode()
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $code = '';
        for ($i = 0; $i < 5; $i++) {
            $code .= $characters[rand(0, $charactersLength - 1)];
        }

        return $code;
    }

    public function mySendMail(
        $subject = null,
        $userId = null,
        $code = null,
        $userEmail = null,
        $controller = null,
        $userName = null
    ) {
        $check = [$subject, $userId, $code, $userEmail, $controller, $userName];
        for ($i = 0; $i < count($check); $i++) {
            if ($check[$i] === null) {
                return false;
            }
        }

        if (
            $_SERVER['SERVER_NAME'] === 'localhost' 
            || $_SERVER['SERVER_NAME'] === '127.0.0.1'
        ) {
            $path = $_SERVER['SERVER_NAME'] . '/' . basename(dirname(APP)) . '/' . $controller;
        } else {
            $path = $_SERVER['SERVER_NAME'] . '/' . $controller;
        }
        $path .= $userId.'/'.$code;
        $message = 'Hi <b>' . $userName . '</b>,<br>';
        $message .= 'Your ' . strtolower($subject) . ' is: <a href="'.$path.'">' . $code . '</a>.';
        $mailer = new Mailer('gmail');
        return $mailer->setFrom(['jptransfiguracion.yns@gmail.com' => 'Microblog by JPT'])
            ->setTransport('gmail')
            ->setEmailFormat('html')
            ->setTo($userEmail)
            ->setSubject($subject)
            ->deliver($message);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null|void Redirects on successful add, renders view otherwise.
     */

    public function add()
    {
        $user = $this->Users->newEmptyEntity();

        if ($this->request->is('post')) {
            $userData = $this->request->getData();

            if ($this->request->getData('password') !== $this->request->getData('confirm_password')) {
                $this->Flash->error(__('The password is not match.'));
            }

            $userPhoto = $this->request->getData('photo');
            $name = $userPhoto->getClientFilename();
            $type = $userPhoto->getClientMediaType();
            $uniqPhotoName = uniqid() . '-' . $name;
            $targetPath = WWW_ROOT. 'img'. DS . 'photos'. DS. $uniqPhotoName;
            $isPhotoSaved = 'We only allowed jpeg, jpg and png.';
            if (
                $type == 'image/jpeg'
                || $type == 'image/jpg'
                || $type == 'image/png'
            ) {
                if (!empty($name)) {
                    if (
                        $userPhoto->getSize() > 0
                        && $userPhoto->getError() == 0
                    ) {
                        $userPhoto->moveTo($targetPath); 
                        $userData['photo'] = $uniqPhotoName;
                        $isPhotoSaved = '';
                    }
                }
            }

            $user = $this->Users->patchEntity($user, $userData);
            $user->user_name = htmlentities($this->request->getData('user_name'));
            $user->full_name = htmlentities($this->request->getData('full_name'));
            $user->email = htmlentities($this->request->getData('email'));
            $user->status = 0;
            $user->is_deleted = 0;
            $code = $this->generateCode();
            $user->activation_code = $code;
            $user->code_creation = date('Y-m-d H:i:s');

            $userEmail = $this->request->getData('email');

            if ($result = $this->Users->save($user)) {
                $subject = 'Activation Code';
                $userId = $result->id;
                $code = $result->activation_code;
                $userEmail = $result->email;
                $userName = $result->user_name;
                $controller = 'users/validate/';

                $this->mySendMail($subject, $userId, $code, $userEmail, $controller,$userName);
               
                $this->Flash->success(__('Your account has been saved. Please activate now to start using MicroblogByJPT'));

                return $this->redirect(['controller'=>'Users','action' => 'validate', $result->id]);
            }

            $this->Flash->error(__('The user could not be saved. Please, try again. ' . $isPhotoSaved));
        }
        $this->set(compact('user'));
    }
    
    /**
    * Validate method
    *
    * @param string|null $id User id.
    * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
    * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
    */
   public function validate($id = null, $activates = null)
   {
        $user = $this->Users->get($id, [
            'contain' => [],
        ]);

        $activate = false;
        if ($activates !== null) {
            if ($user['activation_code'] === $activates) {
                $codeCreation = new Time($user->code_creation);
                if (!$codeCreation->wasWithinLast('4 hours')) {
                    return $this->Flash->error(__('The code is already expired. Request a new one.'));
                }
                $activate = true;
            }
        }

        if ($this->request->is('put')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            
            if ($this->request->getData('submit') === 'resend') {
                $code = $this->generateCode();
                $user->activation_code = $code;
                $user->code_creation = date('Y-m-d H:i:s');
                
                if ($result = $this->Users->save($user)) {
                    $subject = 'Activation Code';
                    $userId = $result->id;
                    $code = $result->activation_code;
                    $userEmail = $result->email;
                    $userName = $result->user_name;
                    $controller = 'users/validate/';
    
                    $this->mySendMail($subject, $userId, $code, $userEmail, $controller, $userName);
                    
                    $this->set(compact('user'));
                    return $this->Flash->success(__('Your activation code has been resend.'));
                }

            }

            if ($user['activation_code'] !== $this->request->getData('input_activation_code')) {

                $this->Flash->error(__('The activation code is incorrect.'));
                return $this->redirect(['controller'=>'Users','action' => 'validate', $user['id']]);
            }

            $codeCreation = new Time($user->code_creation);
            if (!$codeCreation->wasWithinLast('4 hours')) {
                $this->Flash->error(__('The code is already expired. Request a new one.'));
                return $this->redirect(['controller'=>'Users','action' => 'validate', $user['id']]);
            }
            $activate = true;
        }

        if ($activate) {
            $user->status = 1;
            $user->date_activated = date('Y-m-d H:i:s');

            if ($this->Users->save($user)) {
                $this->Flash->success(__('Your account has been activated. Go login your credentials.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        
        $this->set(compact('user'));
   }
 
    /**
     * Edit method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $user = $this->Users->get($id);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());

            $loggedUserId = $this->loggedUser()->id;

            if ($loggedUserId !== intval($id)) {
                return $this->redirect($this->referer());
            }

            if ($this->request->getData('password') !== $this->request->getData('confirm_password')) {

                return $this->Flash->error(__('Password and confirm password not match.'));
            }
            
            $user->user_name = htmlentities($this->request->getData('user_name'));
            $user->full_name = htmlentities($this->request->getData('full_name'));
            $user->email = htmlentities($this->request->getData('email'));
            $user->photo = $user->photo;
            $userPhoto = $this->request->getData('photo');
            if ($name = $userPhoto->getClientFilename() !== '') {
                $type = $userPhoto->getClientMediaType();
                $uniqPhotoName = uniqid() . '-' . $name;
                $targetPath = WWW_ROOT. 'img'. DS . 'photos'. DS. $uniqPhotoName;
                $isPhotoSaved = 'We only allowed jpeg, jpg and png.';
                if (
                    $type == 'image/jpeg'
                    || $type == 'image/jpg'
                    || $type == 'image/png'
                ) {
                    if (!empty($name)) {
                        if (
                            $userPhoto->getSize() > 0
                            && $userPhoto->getError() == 0
                        ) {
                            $userPhoto->moveTo($targetPath); 
                            $user->photo = $uniqPhotoName;
                            $isPhotoSaved = '';
                        }
                    }
                }
            }

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been saved.'));

                return $this->redirect(['action' => 'view', $user->id]);
            }
            $this->Flash->error(__('The user could not be saved. Please, try again.'));
        }
        $this->set(compact('user'));
    }

    /**
     * Delete method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $user = $this->Users->get($id);
        if ($this->Users->delete($user)) {
            $this->Flash->success(__('The user has been deleted.'));
        } else {
            $this->Flash->error(__('The user could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
 
    /**
     * Deactivate method
     *
     * @param string|null $id User id.
     * @return \Cake\Http\Response|null|void Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function deactivate($id = null)
    {
        $user = $this->Users->get($id);
        
        $loggedUserId = $this->loggedUser()->id;
        if ($loggedUserId !== intval($id)) {
            return $this->redirect($this->referer());
        }

        if ($this->request->is(['patch', 'post', 'put'])) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $user->status = 2;

            if ($this->Users->save($user)) {
                $this->Flash->success(__('The user has been deactivated.'));

                return $this->redirect(['action' => 'logout']);
            }
            $this->Flash->error(__('The post could not be deactivated. Please, try again.'));
        }

        $users = $this->Posts->Users->find('list', ['limit' => 200]);
        $repostUsers = $this->Posts->RepostUsers->find('list', ['limit' => 200]);
        //$this->set(compact('post', 'users', 'repostUsers'));
        $this->set(compact('post', 'users'));
    }

    public function forgotPassword($id = null, $verificationCode = null)
    {        
        $user = $this->Users->newEmptyEntity();
        $forPasswordChange = false;

        if ($this->request->is('put')) {
            $requestAction = $this->request->getData('submit');
            $userEmail = $this->request->getData('email');
            $user = $this->Users->find()
                ->where([
                    'email' => $userEmail
                ])
                ->first();
            if ($requestAction === 'forgot') {

                if ($user === null) {
                    $this->set(compact('user','forPasswordChange'));
                    return $this->Flash->error(__('The email doesn\'t belong to any account.'));
                }

                if ($user->status !== 1) {
                    $this->set(compact('user', 'forPasswordChange'));
                    return $this->Flash->error(__('Deactivated or unverified account can\'t use password recovery.'));
                }

                $code = $this->generateCode();
                $user->activation_code = $code;
                $user->code_creation = date('Y-m-d H:i:s');

                if ($result = $this->Users->save($user)) {
                    $subject = 'Verification Code';
                    $userId = $result->id;
                    $code = $result->activation_code;
                    $userEmail = $result->email;
                    $userName = $result->user_name;
                    $controller = 'users/forgot-password/';
    
                    $this->mySendMail($subject, $userId, $code, $userEmail, $controller, $userName);
                    
                    $forPasswordChange = true;
                    $this->set(compact('user','forPasswordChange'));
                    return $this->Flash->success(__('Your verification code has been sent.'));
                }
                return $this->redirect(['action' => 'login']);
            }

            if ($requestAction === 'changePassword') {
                $forPasswordChange = true;
                $this->set(compact('user','forPasswordChange'));

                if (strlen($this->request->getData('new_password')) < 6) {
                    return $this->Flash->error(__('Password must be atleast 6 characters.'));
                }
                if (
                    $this->request->getData('new_password')
                    !== $this->request->getData('confirm_new_password')
                ) {
                    return $this->Flash->error(__('New and Confirm password is not match.'));
                }
                if ($this->request->getData('verification_code') !== $user->activation_code) {
                    return $this->Flash->error(__('Incorrect verification code.'));
                }

                $codeCreation = new Time($user->code_creation);
                if (!$codeCreation->wasWithinLast('4 hours')) {
                    return $this->Flash->error(__('The code is already expired. Request a new one.'));
                }

                $user->password = $this->request->getData('new_password');

                if ($result = $this->Users->save($user)) {
                    $this->Flash->success(__('Successfully changed password. Please log in now.'));
                    $this->redirect(['action' => 'login']);
                }
            }

            if ($requestAction === 'resendVerificationCode') {
                $code = $this->generateCode();
                $user->activation_code = $code;
                $user->code_creation = date('Y-m-d H:i:s');

                if ($result = $this->Users->save($user)) {
                    $subject = 'Verification Code';
                    $userId = $result->id;
                    $code = $result->activation_code;
                    $userEmail = $result->email;
                    $userName = $result->user_name;
                    $controller = 'users/forgot-password/';
    
                    $this->mySendMail($subject, $userId, $code, $userEmail, $controller, $userName);
                    
                    $forPasswordChange = true;
                    $this->set(compact('user','forPasswordChange'));
                    return $this->Flash->success(__('Your verification code has been resent.'));
                }
            }
        } else if ($verificationCode !== null) {
            $user = $this->Users->find()
                ->where([
                    'id' => $id
                ])
                ->first();
            if ($user !== null) {
                if ($user->activation_code === $verificationCode) {
                    $user->verification_code = $verificationCode;
                    $forPasswordChange = true;
                    $this->set(compact('user','forPasswordChange'));
                    return $this->Flash->success(__('Please type your new password.'));
                }
            }
            return $this->redirect(['action' => 'login']);
        }

        $this->set(compact('user','forPasswordChange'));
    }
}
