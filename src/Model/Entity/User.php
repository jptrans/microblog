<?php
declare(strict_types=1);

namespace App\Model\Entity;

use Cake\ORM\Entity;
use Cake\Auth\DefaultPasswordHasher;

/**
 * User Entity
 *
 * @property int $id
 * @property string $user_name
 * @property string $full_name
 * @property string $email
 * @property string $photo
 * @property string $password
 * @property \Cake\I18n\FrozenTime $created
 * @property \Cake\I18n\FrozenTime|null $modified
 * @property int $status
 * @property string $activation_code
 * @property \Cake\I18n\FrozenTime|null $date_activated
 * @property \Cake\I18n\FrozenTime|null $last_login_time
 * @property bool|null $is_deleted
 *
 * @property \App\Model\Entity\Comment[] $comments
 * @property \App\Model\Entity\Follow[] $follows
 * @property \App\Model\Entity\Like[] $likes
 * @property \App\Model\Entity\Post[] $posts
 */
class User extends Entity
{
    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'user_name' => true,
        'full_name' => true,
        'email' => true,
        'photo' => true,
        'password' => true,
        'created' => true,
        'modified' => true,
        'status' => true,
        'activation_code' => true,
        'date_activated' => true,
        'last_login_time' => true,
        'is_deleted' => true,
        'comments' => true,
        'follows' => true,
        'likes' => true,
        'posts' => true,
    ];

    /**
     * Fields that are excluded from JSON versions of the entity.
     *
     * @var array
     */
    protected $_hidden = [
        'password',
    ];

    protected function _setPassword($password)
    {
        if (strlen($password) > 0) {
            return (new DefaultPasswordHasher)->hash($password);
        }
    }
}
